package bourg.austin.TextEngine.Room;

import java.util.ArrayList;

import bourg.austin.TextEngine.Action.Action;

public class Room
{
	String text, lockText;
	boolean lockable, locked;
	
	ArrayList<String> choices;
	ArrayList<Action> actions;
	ArrayList<Boolean> isExhausted;
	
	public Room(String text)
	{
		this(text, null);
	}
	
	public Room(String text, String lockText)
	{
		this.text = text;
		this.lockText = lockText;
		this.lockable = (lockText != null);
		
		locked = false;
		
		this.choices = new ArrayList<String>();
		this.actions = new ArrayList<Action>();
		this.isExhausted = new ArrayList<Boolean>();
	}
	
	public Room addAction(String text, Action action)
	{
		choices.add(text);
		actions.add(action);
		isExhausted.add(false);
		
		return this;
	}
	
	public Action getAction(int index)
	{
		return actions.get(index);
	}
	
	public String getText()
	{
		return text;
	}
	
	public String getLockText()
	{
		return lockText;
	}
	
	public void lock()
	{
		if (lockable)
			locked = true;
	}
	
	public ArrayList<String> getChoices()
	{
		return choices;
	}
	
	public boolean isLocked()
	{
		return locked;
	}
	
	public void displayRoom()
	{
		System.out.println(text);
		for (int i = 0; i < choices.size(); i++)
			System.out.println((i + 1) + ". " + choices.get(i));
	}
}
