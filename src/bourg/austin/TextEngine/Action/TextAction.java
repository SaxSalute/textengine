package bourg.austin.TextEngine.Action;

import bourg.austin.TextEngine.Room.Room;
import bourg.austin.TextEngine.System.Tools;

public class TextAction implements Action
{
	private Room oldRoom;
	private String text;
	
	public TextAction(Room oldRoom, String text)
	{
		this.text = text;
		this.oldRoom = oldRoom;
	}
	
	public void runAction()
	{
		System.out.println(text);
		oldRoom.displayRoom();
		oldRoom.getAction(Tools.getChoice(oldRoom) - 1).runAction();
	}
}
