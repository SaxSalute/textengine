package bourg.austin.TextEngine.Action;

public interface Action
{
	void runAction();
}