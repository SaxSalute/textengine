package bourg.austin.TextEngine.Action;

import bourg.austin.TextEngine.Room.Room;
import bourg.austin.TextEngine.System.Tools;

public class EventAction implements Action
{
	private Room newRoom, oldRoom;
	boolean lockOnLeave;
	
	public EventAction(Room newRoom, Room oldRoom, boolean lockOnLeave)
	{
		this.newRoom = newRoom;
		this.oldRoom = oldRoom;
		this.lockOnLeave = lockOnLeave;
	}

	public void runAction()
	{
		if (!newRoom.isLocked())
		{
			try {oldRoom.lock();} catch (NullPointerException e) {}
			newRoom.displayRoom();
			newRoom.getAction(Tools.getChoice(newRoom) - 1).runAction();
		}
		else
		{
			System.out.println(newRoom.getLockText());
			oldRoom.displayRoom();
			oldRoom.getAction(Tools.getChoice(oldRoom) - 1).runAction();
		}
	}
}
