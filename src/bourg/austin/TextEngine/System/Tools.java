package bourg.austin.TextEngine.System;

import java.io.IOException;
import java.util.Scanner;

import bourg.austin.TextEngine.Room.Room;

public class Tools
{
	private static Scanner scanner = new Scanner(System.in);
	
	public static void clear()
	{
		try
		{
			if (System.getProperty("os.name").contains("Windows"))
				Runtime.getRuntime().exec("cls");
			else
				Runtime.getRuntime().exec("clear");
		}
		catch (IOException e)
		{
			for (int i = 0; i < 20; i++)
				System.out.println("");
		}
	}
	
	public static int getChoice(Room room)
	{
		int choice = -1;
		while (choice <= 0 || choice > room.getChoices().size())
		{
			System.out.print("Choice: ");
			choice = scanner.nextInt();
			if (choice <= 0 || choice > room.getChoices().size())
			{
				System.out.println("There " + (room.getChoices().size() == 1 ? "is" : "are") + " only " + room.getChoices().size() + (room.getChoices().size() == 1 ? " choice." : " choices."));
				room.displayRoom();
			}
		}
		return choice;
	}
}
